# Web Component
Proyecto basado en el repositorio https://gitlab.com/didream-master/sytwc/ejercicios-sass en en el que se implementa web components para la creación de una página con una estructura definida en [enunciado.pdf](enunciado.pdf).

## Implementación
Se ha implementado el componente web `AppServiceComponent` cuya implementación se puede encontrar en [src/scripts/app-service.component.ts](src/scripts/app-service.component.ts). Como se puede ver en su implementación, el componente recibe por parámetro el título, la descripción y la url de la imagen del servicio. Aparte de ello, emite el evento `clickService` al clicar sobre el elemento DOM del componente.

Su uso se realiza de la siguiente forma:

```html
<app-service
    imageUrl="dist/img/servicio-ampliacion.png"
    title="Ampliación de equipo"
    description="Nos adaptamos de manera transparente y flexible con tu equipo de desarrollo para que puedas ir a la velocidad que necesitas en cada momento.">
</app-service>
```

Con el fin de conocer qué servicio ha sido seleccionado, en [src/scripts/main.ts](src/scripts/main.ts) se escucha el evento `clickService` de cada servicio del listado.

```typescript
const selectedServiceContainer = document.querySelector('#selected-service');

document.querySelectorAll('app-service').forEach(serviceEle => {
    serviceEle.addEventListener('clickService', (e) => {
        const currentSelectedServiceEle: AppServiceComponent = document.querySelector('app-service.selected');
        if (currentSelectedServiceEle) {
            currentSelectedServiceEle.classList.remove('selected');
        }
        const appService: AppServiceComponent = e.srcElement as AppServiceComponent;

        appService.classList.add('selected');
        selectedServiceContainer.innerHTML = appService.title;
    })
});
```

Para posteriormente enseñar el título del servicio.