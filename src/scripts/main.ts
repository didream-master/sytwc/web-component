'use strict';

(function() {
    const selectedServiceContainer = document.querySelector('#selected-service-container');
    const selectedServiceElement= document.querySelector('#selected-service');

    document.querySelectorAll('app-service').forEach(serviceEle => {
        serviceEle.addEventListener('clickService', (e) => {
            const currentSelectedServiceEle: AppServiceComponent = document.querySelector('app-service.selected');
            if (currentSelectedServiceEle) {
                currentSelectedServiceEle.classList.remove('selected');
            }
            const appService: AppServiceComponent = e.srcElement as AppServiceComponent;

            appService.classList.add('selected');

            selectedServiceContainer.classList.remove('hidden');
            selectedServiceElement.innerHTML = appService.title;
        })
    });
})();
