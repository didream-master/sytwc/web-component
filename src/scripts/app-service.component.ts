'use strict';

class AppServiceComponent extends HTMLElement {

    private clickEventEmitter = new Event('clickService');

    constructor() {
        super();

        //const shadow = this.attachShadow({mode: 'open'});
        //const template = document.createElement('div');
        
        const title = this.title,
        description = this.description,
        imageUrl = this.imageUrl;

        this.innerHTML = `
        <div class="service-container">
            <div class="service-image-container">
                <img class="service-image" src="${imageUrl}">
            </div>
            <div class="service-title">
                ${title}
            </div>
            <div class="service-description">${description}</div>
        </div>
        `;

        this.addEventListener('click', e => this.onServiceClick(e));
        //shadow.appendChild(template);
    }

    onServiceClick = (e: MouseEvent) => {
        this.dispatchEvent(this.clickEventEmitter)
    }

    disconnectedCallback() {
        console.log('disconnectedCallback', this);
    }

    get title() {
        return this.getAttribute('title');
    }

    get description() {
        return this.getAttribute('description');
    }

    get imageUrl() {
        return this.getAttribute('imageUrl');
    }
}

(function() {
    window.customElements.define('app-service', AppServiceComponent);
})();
